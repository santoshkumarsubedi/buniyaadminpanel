/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
   //showdata();
    $("#Add_User_Button").click(function(e){
        addUserForm();
    });
     $("#View_User_Button").click(function(e){
        viewUser();     
    });
    
    $("#addUserFormSubmit").click(function(e){
        e.preventDefault();
       addUserData();
    });
    
     $("#View_Report_Button").click(function(e){
        viewReport();
    });
    
    $("#Add_Announcement_Button").click(function (e){
        addAnnouncement();
    });
    $("#addAnnouncementFormSubmit").click(function(e){
        e.preventDefault();
        addAnnouncementData();
    });
    
    
});

//function to load add User Form
function addUserForm(){
    $.ajax({
            type:"GET",
            
            url:"/AdminPanel/addUser",
            success:function(result){
                $("#MainContent").html(result)
            },
             error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.responseText);
        alert(thrownError);
      }     
        });
}

//function to load add announcement form
function addAnnouncement(){
    $.ajax({
       type:"GET",
       url:"/AdminPanel/addAnnouncement",
       success:function(result){
           $("#MainContent").html(result)
       },
        error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.responseText);
        alert(thrownError);
      }
    });
}

//view report function to load all the report 
function viewReport(){
    $.ajax({
       type:"GET",
       url:"/AdminPanel/viewReport",
       success:function(result){
           $("#MainContent").html(result)
       },
             error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.responseText);
        alert(thrownError);
      }
    });
}

//View User function to load all the users
function viewUser(){
    $.ajax({
            type:"GET",
            
            url:"/AdminPanel/viewUser",
            success:function(result){
                $("#MainContent").html(result)
            },
             error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.responseText);
        alert(thrownError);
      }
            
        });
}


//function to add user data to database
function addUserData(){
     var data = {};
     var flag = false;
     if($("#username").val().length<=5){
         alert("Username must be longer or equal to 5");
         return;
     }
     if($("#department_id").val()<=0){
         alert("Department is not selected");
         return;
     }
     if($("#firstName").val().lenght<2){
         alert("firtst name is not valid");
         return;
     }
     if($("#lastName").val().lenght<2){
         alert("last name is not valid");
         return;
     }
        data['id']=$("#id").val();
        data['username'] = $("#username").val();
        data['department_id'] = $("#department_id").val();
        data['firstName'] = $("#firstName").val();
        data['middleName'] = $("#middleName").val();
        data['lastName'] = $("#lastName").val();
        data['email'] = $("#email").val();
        data['contactNumber'] = $("#contactNumber").val();
        data["address"] = $("#address").val();
        if(data['id']=='0'){
        $.ajax({
            contentType : 'application/json; charset=utf-8',
            type:'POST',
            url:'/AdminPanel/add',
            data:JSON.stringify(data),
            success:function(result){
               $("#MainContent").html(result)
            }
        });
    }else{
        $.ajax({
            contentType : 'application/json; charset=utf-8',
            type:'POST',
            url:'/AdminPanel/user/editUser',
            data:JSON.stringify(data),
            success:function(result){
               $("#MainContent").html(result)
            }
        });
    }
}
function addAnnouncementData(){
    var data = {};
    data['announcement'] = $("#notice").val();
    data['subject'] = $("#subject").val();
    
    console.log(data);
    $.ajax({
            contentType : 'application/json; charset=utf-8',
            type:'POST',
            url:'/AdminPanel/addAnnouncementForm',
            data:JSON.stringify(data),
            success:function(result){
               $("#MainContent").html(result)
            },
             error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.responseText);
        alert(thrownError);
    }
    });
}

   function showdata(){
            $("#online-box").append("    <div class=\"row\">"+
                          "<div class=\"col-xl-8 col-lg-7\">Santosh Kumar subedi</div>"+
                          "<div class=\"col-xl-2 col-lg-2\"></div>"+
                          "<div class=\"col-xl-2 col-lg-2\">1 min</div>"+
                      "</div>");
              
              $("#1").remove();
        }

