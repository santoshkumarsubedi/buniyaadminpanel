<%-- 
    Document   : viewReport
    Created on : Feb 25, 2020, 7:01:11 PM
    Author     : accidental-genius
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<body>
    <table id ="reportTable">
        <thead>
            <tr>
                <td>S.N</td>
                <td>Subject</td>
                <td>Notice</td>
                <td>Date</td>
                
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${datas}" var="p">
                <tr>
                <td><c:out value="${p.id}"/></td>
                    <td><c:out value="${p.subject}"/></td>
                    <td><c:out value="${p.announcement}"/></td>
                    <td><c:out value="${p.date}"/></td>
                    <td><a href="department/delete?id=${p.id}">Read</a></td>
                    </tr>
            </c:forEach>
        </tbody>
        <tfoot>
            <tr>
                <td>S.N</td>
                <td>Subject</td>
                <td>Notice</td>
                <td>Date</td>
                
            </tr>
        </tfoot>
    </table>
</body>
<script type="text/javascript">
    $(document).ready(function(){
       var table = $("#reportTable").dataTable({
           
       }); 
    });
</script>
