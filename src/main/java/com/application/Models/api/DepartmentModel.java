/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.Models.api;

import java.util.List;

/**
 *
 * @author accidental-genius
 */
public class DepartmentModel {

    private int department_id;
    private String department_name;
    private List<UserModelApi> userModels;

    public int getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public List<UserModelApi> getUserModels() {
        return userModels;
    }

    public void setUserModels(List<UserModelApi> userModels) {
        this.userModels = userModels;
    }

}

 