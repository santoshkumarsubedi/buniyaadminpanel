/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.Models;

/**
 *
 * @author accidental-genius
 */
public class AnnouncementModel {
    private int id;
    
    private String subject;
    
    private String notice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNotice() {
        return notice;
    }

    public void setAnnouncement(String notice) {
        this.notice = notice;
    }
    
    
    
}
