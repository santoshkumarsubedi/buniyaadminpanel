/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author accidental-genius
 */
@Controller
public class AdminController {
    @Autowired
    InMemoryUserDetailsManager inuser;
    
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public String defaultPage(ModelAndView model){
        return "login";
    }
    
    @RequestMapping(value="/403",method=RequestMethod.GET)
    public ModelAndView errorPage(){
        ModelAndView model = new ModelAndView();
        model.setViewName("403");
        return model;
    }
    

    
}
