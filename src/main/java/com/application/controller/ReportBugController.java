/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller;

import com.application.Models.ReportModel;
import com.application.Service.BugReportService;
import com.application.Service.UserService;
import com.application.entity.BugReport;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author accidental-genius
 */
@Controller
public class ReportBugController {
    
    private BugReportService bugReportService;
    
    @Autowired
    @Qualifier("bugReportService")
    public void setBugReportService(BugReportService bugReportService){
        this.bugReportService = bugReportService;
    }
    
    @RequestMapping(value = "/viewReport" , method = RequestMethod.GET)
    public String viewReport(Model model){
        List<BugReport> list = bugReportService.getReports();
        List<ReportModel> lists = new ArrayList<>();
        for(BugReport report:list){
           ReportModel mod = new ReportModel();
           mod.setId(report.getId());
           mod.setSubject(report.getSubject());
           mod.setDate(report.getDate().toString());
           mod.setReporter(report.getReporter().getUsername()); 
           lists.add(mod);
        }
        model.addAttribute("datas", lists);
        return "viewReport";
        
    }
    
    
    
}
