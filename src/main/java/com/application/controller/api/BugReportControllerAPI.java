package com.application.controller.api;

import com.application.ApiRequestPOJO.BugReportRequestPOJO;
import com.application.Service.BugReportService;
import com.application.Service.UserService;
import com.application.entity.BugReport;
import com.application.entity.Users;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author accidental-genius
 */
@RestController
@RequestMapping("/api/bug/")
public class BugReportControllerAPI {
    
    private UserService userService;
    
    private BugReportService bugReportService;
    
    @Autowired
    @Qualifier("userService")
    public void setUserService(UserService userService){
        this.userService = userService;
    }
    
    @Autowired
    @Qualifier("bugReportService")
    public void setBugReportService(BugReportService bugReportService){
        this.bugReportService = bugReportService;
    }
    
    @RequestMapping(value = "insertBug",method = RequestMethod.POST)
    public String insertBug(@RequestBody BugReportRequestPOJO data) throws ParseException{
        String username = data.getUsername();
        Users user = userService.getUserByUsername(username);
        Map<String,String> response = new HashMap<>();
        if(user!=null){
            String date = data.getDate();
           Date dateS = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").parse(date);
            String subject = data.getSubject();
            String report = data.getReport();
            BugReport bugReport = new BugReport();
            bugReport.setDate(dateS);
            bugReport.setReporter(user);
            bugReport.setSubject(subject);
            bugReport.setReport(report);
            bugReportService.insert(bugReport);
            response.put("Status", "Success");
        }else{
            response.put("Status", "Failed");
        }
        return new Gson().toJson(response);
    }
    
}
