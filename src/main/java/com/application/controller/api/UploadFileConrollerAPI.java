/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller.api;
import com.application.Models.api.FileModel;
import com.application.Service.SharedFileService;
import com.application.Service.UserDetailsService;
import com.application.Service.UserService;
import com.application.entity.SharedFile;
import com.application.entity.Users;
import com.application.util.ImageCompressor;
import com.google.gson.Gson;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author accidental-genius
 */
@RestController
@RequestMapping(value = "/api")
public class UploadFileConrollerAPI {
    
    private SharedFileService fileService;
    
    @Autowired
    @Qualifier("fileShareService")
    public void setfileService(SharedFileService sharedFileService){
        this.fileService = sharedFileService;
    }
    
        private UserService userService;
    
    @Autowired
    @Qualifier(value="userService")
    public void setUserService(UserService userService){
        this.userService = userService;
    }
    
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadFileHandler(@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file,
                        @RequestParam("username") String username) {
            HashMap<String,String> response = new HashMap<>();
                        System.out.println("hit");
                        System.out.println(username);
                        System.out.println(name);
                        name = username+".jpg";
                       
		if (!file.isEmpty()) {
                    System.out.println("file found");
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
                                String path = dir.getAbsolutePath()
						+ File.separator + name;
                                System.out.println(path);
                                
                                ImageCompressor compressor = new ImageCompressor(path,name);
                                compressor.start();
				response.put("success", "File Uploaded");
			} catch (Exception e) {
				response.put("success", "Failed");
			}
		} else {
			response.put("success", "No File Received");
		}
                return new Gson().toJson(response);
	}
        
        @RequestMapping(value = "/uploadFileShare", method = RequestMethod.POST)
	public @ResponseBody String uploadFileShare(@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file,
                        @RequestParam("sender") String sender,
                        @RequestParam("receiver") String receiver,
                        @RequestParam("extension") String extension) {
            HashMap<String,String> response = new HashMap<>();
                        System.out.println("hit");
                        System.out.println(sender);
                        System.out.println(name);
                         SharedFile sharedFile = new SharedFile();
                        sharedFile.setDate(new Date());
                        sharedFile.setExtension(extension);
                        sharedFile.setFilename(name);
                        Users user = userService.getUserByUsername(receiver);
                        sharedFile.setReceiver(user);
                        user = userService.getUserByUsername(sender);
                        sharedFile.setSender(user);
                        
		if (!file.isEmpty()) {
                    System.out.println("file found");
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
                                String path = dir.getAbsolutePath()
						+ File.separator + name;
                                System.out.println(path);
                                fileService.insert(sharedFile);
				response.put("success", "File Uploaded");
			} catch (Exception e) {
				response.put("success", "Failed");
			}
		} else {
			response.put("success", "No File Received");
		}
                return new Gson().toJson(response);
	}
        
        
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@RequestParam(value = "name",required=false,defaultValue ="default") String name) throws IOException {
           System.out.println("PHOTO USERNAME"+name);
           byte[] encoded=null;
           try{
    encoded = Files.readAllBytes(Paths.get("/home/accidental-genius/images/"+name+".jpg"));
           }catch(Exception e){
             encoded = Files.readAllBytes(Paths.get("/home/accidental-genius/images/default.jpg"));  
           }finally{
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.IMAGE_JPEG);
    return new ResponseEntity<byte[]>(encoded, headers,HttpStatus.OK);
           }
        
    }
    
    @RequestMapping(value = "/getAllFiles",method = RequestMethod.POST)
    public String getFileNames(@RequestParam("username") String name){
        Users user = userService.getUserByUsername(name);
        List<SharedFile> files = fileService.getByUser(user);
        HashMap<String,Object> response = new HashMap<>();
        List<FileModel> responsedata = new ArrayList<>();
        for(SharedFile file:files){
            FileModel f = new FileModel();
            f.setDate(file.getDate().toString());
            f.setExtension(file.getExtension());
            f.setFileName(file.getFilename());
            f.setId(file.getId());
            f.setReceiver(file.getReceiver().getUsername());
            f.setSender(file.getSender().getUsername());
            responsedata.add(f);    
        }
        response.put("data", responsedata);
        return new Gson().toJson(response);
}

        @RequestMapping(value = "/getFile", method = RequestMethod.GET)
    public void getFile(@RequestParam(value = "id",required=false,defaultValue ="0") String id,HttpServletResponse response) throws IOException {
           SharedFile file = fileService.getId(Integer.parseInt(id));
           String rootPath = System.getProperty("catalina.home");
           File dir = new File(rootPath + File.separator + "tmpFiles"+File.separator+file.getFilename());
           response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename="+file.getFilename());
           try{
               
               Files.copy(dir.toPath(), response.getOutputStream());
//               InputStream is = new FileInputStream(dir);
//               IOUtils.copy(is, response.getOutputStream());
//               response.flushBuffer();
response.getOutputStream().flush();
           }catch(Exception e){
               System.out.println(e.getMessage());
           }finally{
  }
        
    }
    
}
