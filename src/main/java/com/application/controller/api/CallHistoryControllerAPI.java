/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller.api;

import com.application.Models.api.CallHistoryDataModel;
import com.application.Models.api.CallHistoryModel;
import com.application.Service.CallHistoryService;
import com.application.Service.UserService;
import com.application.entity.CallHistory;
import com.application.entity.Users;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ac
 */
 
@RestController
@RequestMapping("/api/history/")
public class CallHistoryControllerAPI {
    
    private CallHistoryService callHistoryService;
    
    private UserService userService;
    
    @Autowired
    @Qualifier("userService")
    public void setUserService(UserService userService){
        this.userService = userService;
    }
    
    @Autowired
    @Qualifier("callHistoryService")
    public void setCallHistoryService(CallHistoryService callHistoryService){
        this.callHistoryService = callHistoryService;
    }
    
    @RequestMapping(value = "insertPersonalCallHistory",method = RequestMethod.POST)
    public String insertPersonalHistory(@RequestBody String data){
        Gson gson = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm:ss").create();
        HashMap<String,String> response = new HashMap<>();
        try{
           CallHistoryModel callHistoryModel = gson.fromJson(data, CallHistoryModel.class);
           CallHistoryDataModel callHistoryDataModel = callHistoryModel.getLogData();
            CallHistory history = new CallHistory();
            history.setDate(callHistoryDataModel.getDate());
            Users caller = userService.getUserByUsername(callHistoryDataModel.getCaller());
            history.setCaller(caller);
            Users receiver = userService.getUserByUsername(callHistoryDataModel.getReceiver());
            history.setReceiver(receiver);
            history.setPort(Integer.parseInt(callHistoryDataModel.getPort()));
            history.setIpAddress(callHistoryDataModel.getIpAddress());
            callHistoryService.insert(history);
            response.put("Status", "Success");
        
        }catch(Exception ex){
            ex.printStackTrace();
            response.put("Status", "Failed");
        }
        return new Gson().toJson(response);
        
    }
    
    @RequestMapping(value = "getHistoryInfo",method = RequestMethod.POST)
    public String getHistoryInfo(@RequestBody String username,HttpServletRequest request){   
        Gson gson = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm:ss").create();
        HashMap<String,String> response = new HashMap<>();
        username = username.substring(1, username.length()-1);
        Users caller = userService.getUserByUsername(username);
        List<CallHistory> lists = callHistoryService.getHistoryByUser(caller);
        List<CallHistoryDataModel> datas = new ArrayList<CallHistoryDataModel>();
        for(CallHistory history:lists){
            CallHistoryDataModel model = new CallHistoryDataModel();
            model.setId(history.getId());
            model.setDate(history.getDate());
            model.setCaller(history.getCaller().getUsername());
            model.setReceiver(history.getReceiver().getUsername());
            model.setIpAddress(history.getIpAddress());
            model.setPort(String.valueOf(history.getPort()));
            datas.add(model);
        }
        HashMap<String,List<CallHistoryDataModel>> d = new HashMap<>();
        System.out.println(datas.size());
        d.put("data", datas);
        return new Gson().toJson(d);
    }
    
    
}
