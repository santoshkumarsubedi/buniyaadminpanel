/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller.api;

import com.application.Models.api.DepMessageDataModel;
import com.application.Models.api.DepMessageModel;
import com.application.Models.api.PersonalMessageDataModel;
import com.application.Models.api.PersonalMessageModel;
import com.application.Service.DepartmentMessageService;
import com.application.Service.DepartmentService;
import com.application.Service.PersonalMessageService;
import com.application.Service.UserService;
import com.application.entity.Department;
import com.application.entity.DepartmentMessages;
import com.application.entity.PersonalMessage;
import com.application.entity.Users;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author accidental-genius
 */
@RestController
@RequestMapping("/api/department-message")
public class DepartmentMessageControllerAPI {
    
        private DepartmentService departmentService;
    
        private UserService userService;
        
        private DepartmentMessageService departmentMessageService;
        
        private PersonalMessageService personalMessageService;
        
           @Autowired
    @Qualifier(value="userService")
    public void setUserService(UserService userService){
        this.userService = userService;
    }
    
    @Autowired
    @Qualifier("departmentService")
    public void setDepartmentService(DepartmentService departmentService){
        this.departmentService = departmentService;
    }
    
    @Autowired
    @Qualifier("departmentMessageService")
    public void setDepartmentMessageService(DepartmentMessageService departmentMessageService){
        this.departmentMessageService = departmentMessageService;
    }
    
    @Autowired
    @Qualifier("personalMessageService")
    public void setPersonalMessageService(PersonalMessageService personalMessageService){
        this.personalMessageService = personalMessageService;
    }
    
    @RequestMapping(value = "/insert" , method = RequestMethod.POST,produces="application/json")
    public String add(@RequestBody String data){
        Gson gson = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm:ss").create();
        System.out.println(data);
        try{
        DepMessageModel msg=gson.fromJson(data, DepMessageModel.class);
        DepMessageDataModel dep = msg.getrData();
            System.out.println(dep.getDate());
            DepartmentMessages departmentMessages = new DepartmentMessages();
            departmentMessages.setDate(dep.getDate());
            departmentMessages.setMessage(dep.getMessage());
            Users user = userService.getUserByUsername(dep.getSender());
            departmentMessages.setSender(user);
            Department department = departmentService.getDepartmentByName(dep.getDepartment());
            departmentMessages.setDepartment(department);
            departmentMessageService.insert(departmentMessages);
            System.out.println("Insert into table");
        }catch(Exception ex){
            System.out.println(ex.getMessage()); 
            ex.printStackTrace();
            return "{\"Status\":\"failed\",\"Response\":\""+ex.getMessage()+"\"}";
        }
        System.out.println();
        return "{\"Status\":\"success\"}";
    }
        
    @RequestMapping(value = "/personal-message" , method = RequestMethod.POST,produces="application/json")
    public String add_personal(@RequestBody String data){
          Gson gson = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm:ss").create();
        System.out.println(data);
        HashMap<String,String> response = new HashMap<>();
        try{
            PersonalMessageModel pData = gson.fromJson(data,PersonalMessageModel.class);
            PersonalMessageDataModel personalData = pData.getrData();
            PersonalMessage pMessage = new PersonalMessage();
            pMessage.setDate(personalData.getDate());
            pMessage.setDeliver(personalData.isDeliver());
            Users receiver = userService.getUserById(Integer.parseInt(personalData.getReceiverId()));
            pMessage.setReceiver(receiver);
            Users sender = userService.getUserById(Integer.parseInt(personalData.getSenderId()));
            pMessage.setReceiver(sender);
            pMessage.setMessage(personalData.getMessage());
            personalMessageService.addMessage(pMessage);
            response.put("status", "success");
        }catch(Exception ex){
            response.put("status","failed");
            response.put("error",ex.getMessage());
        }
        return new Gson().toJson(response);
    }
    
    @RequestMapping(value = "/getOfflineMessage",method = RequestMethod.POST)
    public String getOfflineMessage(@RequestBody MultiValueMap requestData){
        String id = requestData.get("user_id").toString();
        id = id.substring(1,id.length()-1);
       int user_id =(int)Integer.parseInt(id);
       Users user = userService.getUserById(user_id);
       HashMap<String,Object> response = new HashMap<>();
       if(user!=null){
           response.put("Status","Success");
           List<PersonalMessage> messages = personalMessageService.getMessagesByUser(user);
           List<PersonalMessageDataModel> userMessages = new ArrayList<>();
           for(PersonalMessage usr:messages){
            PersonalMessageDataModel pMM = new PersonalMessageDataModel();
            pMM.setDate(usr.getDate());
            pMM.setReceiver(usr.getReceiver().getUsername());
            pMM.setSender(usr.getSender().getUsername());
            pMM.setMessage(usr.getMessage());
            userMessages.add(pMM);
            usr.setDeliver(true);
            personalMessageService.UpdateMessage(usr);
           }
           response.put("data",userMessages);
       }else{
           response.put("Status", "NO_User_Found");
       }
        return new Gson().toJson(response);
    }
}
