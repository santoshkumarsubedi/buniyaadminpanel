/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller.api;

import com.application.Authentication.AuthenticationModule;
import com.application.Models.api.DepartmentModel;
import com.application.Models.api.UserModelApi;
import com.application.Service.DepartmentService;
import com.application.Service.UserDetailsService;
import com.application.Service.UserService;
import com.application.entity.Department;
import com.application.entity.UserDetails;
import com.application.entity.Users;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author accidental-genius
 */
@RestController
@RequestMapping(value="/api/department/")
public class DepartmentControllerAPI {
    
    private DepartmentService departmentService;
    private UserService userService;
    private UserDetailsService userDetailsService;
    
    @Autowired
    @Qualifier(value="userService")
    public void setUserService(UserService userService){
        this.userService = userService;
    }
    @Autowired
    @Qualifier("departmentService")
    public void setDepartmentService(DepartmentService departmentService){
        this.departmentService = departmentService;
    }
    @Autowired
    @Qualifier("userDetailService")
    public void setUserDetailService(UserDetailsService userDetailsService){
        this.userDetailsService = userDetailsService;
    }
    
    @RequestMapping(value = "list",method = RequestMethod.POST)
    public String listDepartment(HttpServletRequest request){
        System.out.println("hit");
    HashMap<String,Object> responseData =new HashMap<>();
        if(AuthenticationModule.verifyToken(request.getHeader("authorization"))){
            System.out.println("authorized");
        List<Department> departments = this.departmentService.listDepartment();
        ArrayList<DepartmentModel> depsarray = new ArrayList<DepartmentModel>();
        for(Department dep:departments){
            DepartmentModel depModel = new DepartmentModel();
            depModel.setDepartment_id(dep.getId());
            depModel.setDepartment_name(dep.getName());
            
            List<Users> users = this.userService.getUsersByDepartment(dep.getId());
            ArrayList<UserModelApi> usermodels = new ArrayList<UserModelApi>();
            for(Users user:users){
                UserDetails userDetails = this.userDetailsService.getDetailByID(user.getId());
                System.out.println(userDetails);
                UserModelApi model = new UserModelApi();
                model.setUserid(user.getId());
                model.setUsername(user.getUsername());
                System.out.println("hit1");
                model.setFullname(userDetails.getFirstName()+" "+userDetails.getLastname());
                System.out.println("hit2");
                usermodels.add(model);  
            }
            depModel.setUserModels(usermodels);
            depsarray.add(depModel);
            responseData.put("Status", "success");
            responseData.put("data", depsarray);
        }}else{
            responseData.put("Status", "failed");
        }
        Gson gson = new Gson();
        System.out.println(gson.toJson(responseData));
        return gson.toJson(responseData);
    }
}
