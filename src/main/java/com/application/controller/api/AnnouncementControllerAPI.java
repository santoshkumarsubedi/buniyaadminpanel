/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller.api;

import com.application.Authentication.AuthenticationModule;
import com.application.Service.AnnouncementService;
import com.application.entity.Announcement;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author accidental-genius
 */
@RestController
@RequestMapping(value = "/api/announcement/")
public class AnnouncementControllerAPI {
        
    private AnnouncementService announcementService;
    
    @Autowired
    @Qualifier("announcementService")
    public void setAnnouncementService(AnnouncementService announcementService){
        this.announcementService = announcementService;
    }
    
    @RequestMapping(value = "getAnnouncement",method = RequestMethod.POST)
    public String getAnnouncement(HttpServletRequest request){
        HashMap<String,Object> response = new HashMap<>();
        if(AuthenticationModule.verifyToken(request.getHeader("authorization"))){
        List<Announcement> announcements = announcementService.getAllAnnouncement();
        response.put("Status", "Success");
        response.put("Data", announcements);
        }else{
            response.put("Status", "Success");
        }
        return new Gson().toJson(response);
    }
}
