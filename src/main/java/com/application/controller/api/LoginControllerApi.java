/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller.api;

import com.application.ApiRequestPOJO.LoginRequestPojo;
import com.application.Authentication.AuthenticationModule;
import com.application.Service.UserDetailsService;
import com.application.Service.UserService;
import com.application.entity.UserDetails;
import com.application.entity.Users;
import com.application.util.KeyGenerator;
import com.google.gson.Gson;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.Key;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.web.savedrequest.Enumerator;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author accidental-genius
 */

@RestController
@RequestMapping(value = "/api")
public class LoginControllerApi {
    private UserService userService;
    private UserDetailsService userDetails;
    
    @Autowired
    @Qualifier(value="userService")
    public void setUserService(UserService userService){
        this.userService = userService;
    }
    
    @Autowired
    @Qualifier("userDetailService")
    public void setUserDetailService(UserDetailsService userDetails){
        this.userDetails = userDetails;
    }
    
    @RequestMapping(value = "/logins",method = RequestMethod.POST)
    public String login(@RequestBody LoginRequestPojo login){
        HashMap<String,String> response = new HashMap<>();
        Users users = this.userService.getUserByUsername(login.getUsername());
        if(users!=null){
        UserDetails userDetails = this.userDetails.getDetailByID(users.getId());
        if(users!=null){
            if(login.getPassword().equals(users.getPassword())){
                response.put("Status", "Success");
                response.put("Username",users.getUsername());
                response.put("UserId",String.valueOf(users.getId()));
                response.put("Fullname",userDetails.getFirstName()+" "+userDetails.getMiddleName()+" "+userDetails.getLastname());
                response.put("DepartmentId", String.valueOf(users.getDepartment().getId()));
                response.put("Token", AuthenticationModule.createToken(String.valueOf(users.getId()),users.getUsername()));
                if(users.isPasswordChanged()){
                    response.put("PasswordStatus", "YES");
                }else{
                    response.put("PasswordStatus", "NO");
                }
            }else{
               response.put("Status", "Failed");
            }
        }else{
            response.put("Status", "Failed");
        }
        }else{
           response.put("Status", "Failed"); 
        }
        return new Gson().toJson(response);
    }
    
//    @RequestMapping(value = "/logins",method = RequestMethod.POST)
//    public String login(@RequestBody MultiValueMap login){
//        Claims claims = getClaims(login);
//        String username = claims.get("username").toString();
//        Users users = this.userService.getUserByUsername(username);
//        HashMap<String,String> response = new HashMap<>();
//        Gson gson = new Gson();
//        if(users!=null){
//            if(claims.get("password").toString().equals(users.getPassword())){
//                String macAddress = claims.get("id").toString();
//                response.put("Status", "Success");
//                if(users.isPasswordChanged()){
//                    response.put("PasswordStatus", "yes");
//                }else{
//                    response.put("PasswordStatus", "no");
//                }
//                if(users.getMacAddress()!=null){
//                    response.put("MACExist", "yes");
//                    if(macAddress==users.getMacAddress()){
//                        response.put("idMatch", "yes");
//                    }else{
//                        response.put("idMatch", "no");
//                    }
//                }else{
//                    response.put("MACExist", "no");
//                }
//                
//            }else{
//               response.put("Status", "Failed");
//            }
//            
//        }else{
//            response.put("Status", "Failed");
//        }
//        return gson.toJson(response);
//    }
    
 /*   @RequestMapping(value = "/verify",method=RequestMethod.POST)
    public String Verify(@RequestBody MultiValueMap verify){
        Claims claims=getClaims(verify);
        String android = claims.get("androidId").toString();
        Users users = this.userService.getUserByDeviceId(android);
        HashMap<String,String> response = new HashMap<>();
        Gson gson = new Gson();
        if(users!=null){
            response.put("username", users.getUsername());
            response.put("verified", "yes");
            response.put("Status","Success");
            
        }else{
            response.put("Status","Failed");
            response.put("verified", "no");
        }
        return gson.toJson(response);
    }
    
    @RequestMapping(value = "/addMac",method = RequestMethod.POST)
    public String addMac(@RequestBody MultiValueMap data){
        Claims claims = getClaims(data);
        String username = claims.get("username").toString();
        String macAddress = claims.get("macAddress").toString();
        Users users = this.userService.getUserByUsername(username);
        users.setMacAddress(macAddress);
        this.userService.updatePerson(users);
        HashMap<String,String> response = new HashMap<>();
         response.put("Status","Success");
         Gson gson = new Gson();
         return gson.toJson(response);
        
    }
*/
    
    @RequestMapping(value = "/changePassword",method=RequestMethod.POST)
    public String changePassword(@RequestBody LoginRequestPojo data,HttpServletRequest request){
        HashMap<String,String> response = new HashMap<>();
            if(AuthenticationModule.verifyToken(request.getHeader("authorization"))){
                String username = data.getUsername();
                String password = data.getPassword();
                Users users = this.userService.getUserByUsername(username);
                users.setPassword(password);
                users.setPasswordChanged(true);
                this.userService.updatePerson(users);
                response.put("Status","Success");
             }else{
                response.put("Status","Failed");
            }
         
         return new Gson().toJson(response);
    }
   
    /*
    public static Claims getClaims(MultiValueMap rawdata){
        String rowdata = rawdata.get("data").toString();
         String data = rowdata.substring(1, rowdata.length()-1);
        KeyGenerator keyGenerator = new KeyGenerator();
        String firstKey=keyGenerator.generateKey();
        Key signingKey = new SecretKeySpec(firstKey.getBytes(), SignatureAlgorithm.HS256.getJcaName());
        Claims claims=null;
        try{
        claims = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(data).getBody();
            System.out.println(claims);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return claims;
    }*/

}
