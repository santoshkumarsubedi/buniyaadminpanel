/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller;

import com.application.Models.AnnouncementModel;
import com.application.Service.AnnouncementService;
import com.application.entity.Announcement;
import com.application.entity.Department;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author accidental-genius
 */
@Controller
public class AnnouncementController {
    
    private AnnouncementService announcementService;
    
    @Autowired
    @Qualifier("announcementService")
    public void setAnnouncementService(AnnouncementService announcementService){
        this.announcementService = announcementService;
    }
    
    @RequestMapping(value = "addAnnouncement",method = RequestMethod.GET)
   public String add(Model model){
       model.addAttribute("announcement", new AnnouncementModel());
       return "addAnnouncement";
   }
   
   @RequestMapping(value = "viewAnnouncement",method = RequestMethod.GET)
   public String viewReport(Model model){
       List<Announcement> list = announcementService.getAllAnnouncement();
       model.addAttribute("datas", list);
       return "viewAnnouncement";
   }
   @RequestMapping(value = "addAnnouncementForm",method = RequestMethod.POST,produces = "application/json")
   public String addData(@RequestBody AnnouncementModel announcement,Model model){
       Announcement data = new Announcement();
       System.out.println(announcement.getNotice());
       System.out.println(announcement.getSubject());
       data.setSubject(announcement.getSubject());
       data.setAnnouncement(announcement.getNotice());
       data.setDate(new Date());
       announcementService.insert(data);
       return "";
   }
}
