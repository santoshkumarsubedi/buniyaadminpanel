/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller;



import com.application.Models.UserModel;
import com.application.Service.BugReportService;
import com.application.Service.DepartmentService;
import com.application.Service.UserDetailsService;
import com.application.Service.UserService;
import com.application.entity.BugReport;
import com.application.entity.Department;
import com.application.entity.UserDetails;
import com.application.entity.Users;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author accidental-genius
 */
@Controller
public class UserController {
    private UserService userService;
    private DepartmentService departmentService;
    private UserDetailsService userDetailService;
    
        private BugReportService bugReportService;
    
    @Autowired
    @Qualifier("bugReportService")
    public void setBugReportService(BugReportService bugReportService){
        this.bugReportService = bugReportService;
    }
    @Autowired
    @Qualifier(value="userService")
    public void setUserService(UserService userService){
        this.userService = userService;
    }

    @Autowired
    @Qualifier(value="departmentService")
    public void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }
    
    @Autowired
    @Qualifier(value = "userDetailService")
    public void setUserDetailService(UserDetailsService userDetailService){
        this.userDetailService = userDetailService;
    }
    
    @RequestMapping(value="/add",method=RequestMethod.POST,produces = "application/json")
    public String Add(@RequestBody UserModel userModel,Model model){
        Users user = new Users();
        System.out.println("user id:"+userModel.getId());
        user.setId(userModel.getId());
        user.setUsername(userModel.getUsername());
        user.setPassword("p@ssword");
        Department department = departmentService.getDepartmentById(userModel.getDepartment_id());
        if(department!=null){
            user.setDepartment(department);
        }
        user.setActivated(true);
        user.setPasswordChanged(false);
        this.userService.addUser(user);
        UserDetails userDetails = new UserDetails();
        userDetails.setFirstName(userModel.getFirstName());
        userDetails.setMiddleName(userModel.getMiddleName());
        userDetails.setLastname(userModel.getLastName());
        userDetails.setAddress(userModel.getAddress());
        userDetails.setContactNumber(userModel.getContactNumber());
        userDetails.setEmail(userModel.getEmail());
        userDetails.setUser(user);
        this.userDetailService.insert(userDetails);
         List<Users> users = this.userService.listUsers();
      model.addAttribute("displayUsers", users);
      return "viewusers";
    }
    
  @RequestMapping(value="/addUser",method=RequestMethod.GET)
  public ModelAndView AddPage(){
     ModelAndView model = new ModelAndView("addUser");
     model.addObject("user", new UserModel());
     List<Department> departments=departmentService.listDepartment();
     model.addObject("departments", departments);
     return model;
  }
  

  
  @RequestMapping(value = "/viewUser",method = RequestMethod.GET)
  public String showUsers(Model model){
      List<Users> users = this.userService.listUsers();
      model.addAttribute("displayUsers", users);
      return "viewusers";
  }
  
  
  @RequestMapping(value="/user/reset",method = RequestMethod.GET)
  public String resetUser(@RequestParam("id") int id,Model model){
      Users user = userService.getUserById(id);
      if(user!=null){
          user.setPassword("p@ssw0rd");
          user.setPasswordChanged(false);
          user.setMacAddress(null);
          this.userService.updatePerson(user);      
      }
      List<Users> users = this.userService.listUsers();
      model.addAttribute("displayUsers", users);
      return "viewusers_1";
  }
  
  @RequestMapping(value="user/edit",method = RequestMethod.GET)
  public ModelAndView editUser(@RequestParam("id") int id) throws IOException{
      ModelAndView model = new ModelAndView("editUser");
      Users user = userService.getUserById(id);
      UserDetails userDetails = userDetailService.getDetailByID(user.getId());
      UserModel userModel = new UserModel();
      userModel.setAddress(userDetails.getAddress());
      userModel.setContactNumber(userDetails.getContactNumber());
      userModel.setEmail(userDetails.getEmail());
      userModel.setFirstName(userDetails.getFirstName());
      userModel.setMiddleName(userDetails.getMiddleName());
      userModel.setLastName(userDetails.getLastname());
      userModel.setUsername(user.getUsername());
      userModel.setId(user.getId());
      model.addObject("user",userModel );
      List<Department> departments=departmentService.listDepartment();
      model.addObject("departments", departments);
      return model;
  }
  
  @RequestMapping(value="user/editUser",method=RequestMethod.POST)
    public String Edit(@RequestBody UserModel userModel,Model model){
        Users user = new Users();
        System.out.println("user id:"+userModel.getId());
        user.setId(userModel.getId());
        user.setUsername(userModel.getUsername());
        user.setPassword("p@ssword");
        Department department = departmentService.getDepartmentById(userModel.getDepartment_id());
        if(department!=null){
            user.setDepartment(department);
        }
        user.setActivated(true);
        user.setPasswordChanged(false);
        this.userService.updatePerson(user);
        UserDetails userDetails = new UserDetails();
        userDetails.setFirstName(userModel.getFirstName());
        userDetails.setMiddleName(userModel.getMiddleName());
        userDetails.setLastname(userModel.getLastName());
        userDetails.setAddress(userModel.getAddress());
        userDetails.setContactNumber(userModel.getContactNumber());
        userDetails.setEmail(userModel.getEmail());
        userDetails.setUser(user);
        UserDetails userDetails1 = this.userDetailService.getDetailByID(user.getId());
        userDetails.setId(userDetails1.getId());
        this.userDetailService.update(userDetails);
         List<Users> users = this.userService.listUsers();
        model.addAttribute("displayUsers", users);
      return "viewusers";
    }

  
    @RequestMapping(value={"/index","/"},method=RequestMethod.GET)
    public String indexPage(Model model) throws IOException{
        List<Users> users = this.userService.listUsers();
        List<BugReport> reports = this.bugReportService.getReports();
        model.addAttribute("totalReports",reports.size());
        model.addAttribute("totalUsers",users.size());
        return "index";
    }
  
    /*@RequestMapping(value={"/test"},method=RequestMethod.GET)
    public String test(Model model) throws IOException{
      
        return "templete";
    }*/
}
