/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.controller;

import com.application.Service.DepartmentService;
import com.application.entity.Department;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author accidental-genius
 */
@Controller
@RequestMapping("department")
public class DepartmentController {
    
    private DepartmentService departmentService;
    
    @Autowired
    @Qualifier(value = "departmentService")
    public void setDepartmentService(DepartmentService departmentService){
        this.departmentService = departmentService;
    }
    
   @RequestMapping(value="",method = RequestMethod.GET)
   public String index(Model model){
       List<Department> departments = departmentService.listDepartment();
       System.out.println(departments.size());
       model.addAttribute("displayDepartments", departments);
       return "viewdepartments";
   }
   @RequestMapping(value = "/add",method = RequestMethod.GET)
   public String add(Model model){
       model.addAttribute("department", new Department());
       
       return "addDepartment";
   }
   
   @RequestMapping(value="/addDep",method = RequestMethod.POST)
   public String addDepartment(@ModelAttribute("department") Department department,Model model){
       System.out.println(department.getId());
       if(department.getId()!=0){
           departmentService.update(department);
           System.out.println("updated");
       }else{
           departmentService.insert(department);
           System.out.println("inserted");
       }
       List<Department> departments = departmentService.listDepartment();
       System.out.println(departments.size());
       model.addAttribute("displayDepartments", departments);
       return "viewdepartments";
   }
   
      @RequestMapping(value="/editDep",method = RequestMethod.POST)
   public String editDepartment(@ModelAttribute("department") Department department,Model model){
       System.out.println("updated:"+department.getId());
       departmentService.update(department);
       
       List<Department> departments = departmentService.listDepartment();
       System.out.println(departments.size());
       model.addAttribute("displayDepartments", departments);
       return "viewdepartments";
   }
   
   @RequestMapping(value="/edit",method = RequestMethod.GET)
  public ModelAndView editDepartment(@RequestParam("id") int id) throws IOException{
      Department department = departmentService.getDepartmentById(id);
      ModelAndView model = new ModelAndView("editDepartment");
      model.addObject("department", department);
      return model;
  }
  
  @RequestMapping(value="/delete",method = RequestMethod.GET)
  public String deleteDepartment(@RequestParam("id") int id,Model model) throws IOException{
      departmentService.DeleteById(id);
       List<Department> departments = departmentService.listDepartment();
       model.addAttribute("displayDepartments", departments);
       return "viewdepartments";
  }
}
