/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.BugReportDAO;
import com.application.entity.BugReport;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author accidental-genius
 */
public class BugReportDAOImpl implements BugReportDAO{
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void insert(BugReport bugReport) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(bugReport);
        session.flush();
    }

    @Override
    public void update(BugReport bugReport) {
        Session session = sessionFactory.getCurrentSession();
        session.update(bugReport);
        session.flush();
    }

    @Override
    public List<BugReport> getReports() {
        Session session = sessionFactory.getCurrentSession();
        List<BugReport> reports = session.createQuery("FROM BugReport").list();
        return reports;
    }
    
}
