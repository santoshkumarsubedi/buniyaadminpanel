/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.DepartmentDAO;
import com.application.entity.Department;
import com.application.entity.Users;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author accidental-genius
 */
public class DepartmentDaoImpl implements DepartmentDAO{
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void insert(Department department) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(department);
        session.flush();
    }

    @Override
    public void update(Department department) {
       Session session = this.sessionFactory.getCurrentSession();
       session.update(department);
       session.flush();
    }

    @Override
    public List<Department> listDepartment() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Department> departments = session.createQuery("from Department").list();
        return departments;
    }

    @Override
    public Department getDepartmentById(int id) {
       Session session = this.sessionFactory.getCurrentSession();
        Department department=(Department)session.get(Department.class, id);
      
      
        return department;
    }

    @Override
    public void DeleteById(int id) {
        Department department = getDepartmentById(id);
       Session session = this.sessionFactory.getCurrentSession();
       session.delete(department);
    }

    @Override
    public Department getDepartmentByName(String departmentName) {
       Session session = this.sessionFactory.getCurrentSession();
       Department department= (Department) session.createQuery("from Department where name=:name").setParameter("name", departmentName).uniqueResult();  
       return department;
    }
    
}
