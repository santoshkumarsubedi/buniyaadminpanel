     /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.AnnouncementDAO;
import com.application.entity.Announcement;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author accidental-genius
 */
public class AnnouncementDAOImpl implements AnnouncementDAO{

    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void insert(Announcement announcement) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(announcement);
        session.flush();
    }

    @Override
    public void update(Announcement announcement) {
        Session session = sessionFactory.getCurrentSession();
        session.update(announcement);
        session.flush();
    }

    @Override
    public List<Announcement> getAllAnnouncement() {
        Session session = sessionFactory.getCurrentSession();
        List<Announcement> lists = session.createQuery("FROM Announcement").list();
        return lists;
        
    }

    @Override
    public Announcement getAnnouncementById(int id) {
       Session session = sessionFactory.getCurrentSession();
       Announcement announcement = (Announcement) session.createQuery("SELECT Announcement where id = :id").setParameter("id", id).uniqueResult();
       return announcement;
    }
    
}
