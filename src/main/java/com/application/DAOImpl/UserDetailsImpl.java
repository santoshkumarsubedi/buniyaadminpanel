/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.UserDetailDAO;
import com.application.Service.UserDetailsService;
import com.application.entity.UserDetails;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author accidental-genius
 */
@Repository
public class UserDetailsImpl implements UserDetailDAO{

    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void insert(UserDetails userDetails) {
      Session session=sessionFactory.getCurrentSession();
      session.persist(userDetails);
      session.flush();
      
    }

    @Override
    public void update(UserDetails userDetails) {
       Session session=sessionFactory.getCurrentSession();
      session.update(userDetails);
      session.flush();
      
    }

    @Override
    public List<UserDetails> list() {
       Session session=sessionFactory.getCurrentSession();
      List<UserDetails> details = session.createQuery("from UserDetails").list();
      return details;
    }

    @Override
    public UserDetails getDetailByID(int id) {
       Session session = this.sessionFactory.getCurrentSession();
       
       UserDetails userDetails = (UserDetails) session.createQuery("from UserDetails where user_id=:id").setParameter("id", id).uniqueResult();
       return userDetails;
    }

    @Override
    public void remove(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
