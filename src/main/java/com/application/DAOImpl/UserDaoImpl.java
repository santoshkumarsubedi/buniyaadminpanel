/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.UserDAO;
import com.application.entity.Users;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import jdk.nashorn.internal.scripts.JO;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author accidental-genius
 */
@Repository
public class UserDaoImpl implements UserDAO{
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addUser(Users user) {
       Session session = this.sessionFactory.getCurrentSession();
       session.persist(user);
       session.flush();
      
    }

    @Override
    public void UpdatePerson(Users user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
        session.flush();
    }

    @Override
    public List<Users> listUsers() {
        Session session = this.sessionFactory.getCurrentSession();
       List<Users> userlist = session.createQuery("from Users").list();
       return userlist;
    }

    @Override
    public Users getUserById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Users user=(Users)session.get(Users.class, id);
        return user;
    }

    @Override
    public void removePerson(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Users getUserByUsername(String username) {
       Session session = this.sessionFactory.getCurrentSession();  
       Users user= (Users) session.createQuery("from Users where username=:username")
                                  .setParameter("username", username).uniqueResult();
       return user;
    }

    @Override
    public Users getUserByDeviceId(String deviceId) {
       Session session = this.sessionFactory.getCurrentSession();
       Users user = (Users) session.createQuery("from Users where macAddress=:macAddress").setParameter("macAddress", deviceId).uniqueResult();
       return user;
    }

    @Override
    public List<Users> getUsersByDepartment(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Users> users= session.createQuery("from Users where department_id=:department_id").setParameter("department_id", id).list();
        return users;
    }
    
    
    
}
