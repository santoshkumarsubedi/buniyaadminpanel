/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.CallHistoryDAO;
import com.application.entity.CallHistory;
import com.application.entity.Users;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author accidental-genius
 */
public class CallHistoryDAOImpl implements CallHistoryDAO{

    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void insert(CallHistory callHistory) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(callHistory);
        session.flush();
    }

    @Override
    public void update(CallHistory callHistory) {
        Session session = sessionFactory.getCurrentSession();
        session.update(callHistory);
        session.flush();
    }

    @Override
    public List<CallHistory> findHistoryByUsrt(Users users) {
        Session session = sessionFactory.getCurrentSession();
        List<CallHistory> histories = session.createQuery("FROM CallHistory where caller = :user OR receiver = :user").setParameter("user", users).list();
        return histories;
    }
    
}
