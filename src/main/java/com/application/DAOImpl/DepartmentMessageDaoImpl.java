/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.DepartmentMessageDAO;
import com.application.entity.DepartmentMessages;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author accidental-genius
 */
public class DepartmentMessageDaoImpl implements DepartmentMessageDAO{

    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void insert(DepartmentMessages message) {
        Session session = this.sessionFactory.getCurrentSession();
       session.persist(message);
       session.flush();
        
    }

    @Override
    public List<DepartmentMessages> getDepartmentById(int id) {
          Session session = this.sessionFactory.getCurrentSession();
        List<DepartmentMessages> messages= session.createQuery("from DepartmentMessages where department=:department_id").setParameter("department_id", id).list();
        return messages;
    }
    
}
