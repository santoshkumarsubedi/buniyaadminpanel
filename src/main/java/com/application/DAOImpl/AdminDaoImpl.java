/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.AdminDao;
import com.application.entity.Admin;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author accidental-genius
 */

@Repository
public class AdminDaoImpl implements AdminDao{

    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public Admin findByUserName(String username) {
      Admin admin = (Admin) this.sessionFactory.getCurrentSession().createQuery("from Admin where username=?").setParameter(0, username).uniqueResult();
      if(admin!=null){
          return admin;
      }else{
          return null;
      }
    }
    
}
