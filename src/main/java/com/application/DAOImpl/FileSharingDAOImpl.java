/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.FileSharingDAO;
import com.application.entity.SharedFile;
import com.application.entity.Users;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author accidental-genius
 */

public class FileSharingDAOImpl implements FileSharingDAO{
    
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void insert(SharedFile file) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(file);
        session.flush();
    }

    @Override
    public SharedFile getId(int id) {
        Session session = sessionFactory.getCurrentSession();
        SharedFile file = (SharedFile) session.createQuery("from SharedFile where id=:id").setParameter("id", id).uniqueResult();
        return  file;
    }

    @Override
    public List<SharedFile> getByUser(Users users) {
        Session session = sessionFactory.getCurrentSession();
        List<SharedFile> files = session.createQuery("from SharedFile where sender=:user or receiver=:user").setParameter("user", users).list();
        return files;
    }
    
}
