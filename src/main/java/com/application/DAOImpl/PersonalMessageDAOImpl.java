/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAOImpl;

import com.application.DAO.PersonalMessageDao;
import com.application.entity.PersonalMessage;
import com.application.entity.Users;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author accidental-genius
 */
@Repository
public class PersonalMessageDAOImpl implements PersonalMessageDao{
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addMessage(PersonalMessage message) {
       Session session = this.sessionFactory.getCurrentSession();
       session.persist(message);
       session.flush();
    }

    @Override
    public void UpdateMessage(PersonalMessage message) {
       Session session = this.sessionFactory.getCurrentSession();
       session.update(message);
       session.flush();
    }

    @Override
    public List<PersonalMessage> getMessagesByUser(Users user) {
       Session session = this.sessionFactory.getCurrentSession();
       List<PersonalMessage> messages = session.createQuery("from PersonalMessage where receiver=:receiver").setParameter("receiver", user).list();
        return messages;
    }
    
}
