/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.Authentication;

import com.application.util.KeyGenerator;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author accidental-genius
 */
public class AuthenticationModule {
    private static String key = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    
    public static String createToken(String id,String username){
        SignatureAlgorithm algorithm = SignatureAlgorithm.HS256;
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        
        byte[] apiKeySecret = DatatypeConverter.parseBase64Binary(AuthenticationModule.key);
        Key signingKey = new SecretKeySpec(apiKeySecret, algorithm.getJcaName());
        
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setSubject(username)
                .setIssuer("Web")
                .signWith(signingKey)
                .setExpiration(new Date(c.getTimeInMillis()));
        return builder.compact();
    }
    
    public static boolean verifyToken(String token){
        try{
            Jwts.parser()         
            .setSigningKey(new SecretKeySpec(DatatypeConverter.parseBase64Binary(AuthenticationModule.key),
                                                SignatureAlgorithm.HS256.getJcaName()))
            .parseClaimsJws(token).getBody();
    return true;
        }catch(Exception ex){
            return false;
        }
}    
}
