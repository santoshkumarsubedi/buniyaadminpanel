/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAO;

import com.application.entity.DepartmentMessages;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface DepartmentMessageDAO {
    public void insert(DepartmentMessages message);
    public List<DepartmentMessages> getDepartmentById(int id);
    
}
