/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAO;

import com.application.entity.Announcement;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface AnnouncementDAO {
    void insert(Announcement announcement);
    void update(Announcement announcement);
    List<Announcement> getAllAnnouncement();
    Announcement getAnnouncementById(int id);
}
