/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAO;

import com.application.entity.UserDetails;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface UserDetailDAO {
    public void insert(UserDetails userDetails);
    public void update(UserDetails userDetails);
    public List<UserDetails> list();
    public UserDetails getDetailByID(int id);
    public void remove(int id);
    
    
    
}
