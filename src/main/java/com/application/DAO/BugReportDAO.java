/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAO;

import com.application.entity.BugReport;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface BugReportDAO {
    void insert(BugReport bugReport);
    void update(BugReport bugReport);
    List<BugReport> getReports();
}
