/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAO;

import com.application.entity.CallHistory;
import com.application.entity.Users;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface CallHistoryDAO {
    void insert(CallHistory callHistory);
    void update(CallHistory callHistory);
    List<CallHistory> findHistoryByUsrt(Users users);
    
}
