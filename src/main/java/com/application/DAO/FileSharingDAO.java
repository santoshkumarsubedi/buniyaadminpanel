/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAO;

import com.application.entity.SharedFile;
import com.application.entity.Users;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface FileSharingDAO {
    public void insert(SharedFile file);
    public SharedFile getId(int id);
    public List<SharedFile> getByUser(Users users);
    
}
