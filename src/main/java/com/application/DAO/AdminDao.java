/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAO;

import com.application.entity.Admin;

/**
 *
 * @author accidental-genius
 */
public interface AdminDao {
    public Admin findByUserName(String username);
}
