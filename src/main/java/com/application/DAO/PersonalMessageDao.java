/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.DAO;

import com.application.entity.PersonalMessage;
import com.application.entity.Users;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface PersonalMessageDao {
    public void addMessage(PersonalMessage message);
    public void UpdateMessage(PersonalMessage message);
    public List<PersonalMessage> getMessagesByUser(Users user);
}
