/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.Service;

import com.application.entity.Department;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface DepartmentService {
     public void insert(Department department);
    public void update(Department department);
    public List<Department> listDepartment();
    public Department getDepartmentById(int id);
    public void DeleteById(int id);
    public Department getDepartmentByName(String departmentName);
}

