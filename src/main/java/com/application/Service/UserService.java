/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.Service;

import com.application.entity.Users;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface UserService {
    public void addUser(Users user);
    public void updatePerson(Users user);
    public List<Users> listUsers();
    public Users getUserById(int id);
    public void removePerson(int id);
    public Users getUserByUsername(String username);
    public Users getUserByDeviceId(String deviceId);
    public List<Users> getUsersByDepartment(int id);
    
}
