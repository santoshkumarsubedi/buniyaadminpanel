/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.Service;

import com.application.entity.BugReport;
import com.application.entity.CallHistory;
import com.application.entity.Users;
import java.util.List;

/**
 *
 * @author accidental-genius
 */
public interface BugReportService {
    void insert(BugReport bugReport);
    void update(BugReport bugReport);
    List<BugReport> getReports();
    
    
}
