/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author accidental-genius
 */
public class SocketThread extends Thread{
    
    static private  Socket socket;
    static private  BufferedReader bufferedReader;
    static public  PrintStream printWriter;
    static private  String result;

    @Override
    public void run() {
        super.run(); 
        connect();
        while(true){
            try {
                String aa = bufferedReader.readLine();
                System.out.println(aa);
            } catch (IOException ex) {
                Logger.getLogger(SocketThread.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
        }
    }
    
    
    public void connect(){
        try {
            socket = new Socket("192.168.100.208",8888);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         
            printWriter = new PrintStream(socket.getOutputStream(),true);
           

        } catch (IOException ex) {
            Logger.getLogger(SocketThread.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }
    }
    
    public static void sendMessage(String message){
        printWriter.print(message);
        printWriter.flush();
    }
}
