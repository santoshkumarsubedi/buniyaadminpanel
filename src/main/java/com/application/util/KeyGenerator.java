/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 */
public class  KeyGenerator {
    public static String generateKey(){
          String allowedChar = "abcdefghijklmnopqrstuvwxyz0123456789+=!@$#";
        String alphabets = "abcdefghijklmnopqrstuvwxyz";
        char[] words = new char[36];
        char[] saltChar = new char[15];
        char[] keyChar = new char[15];
        char[] dateChar = new char[10];
        char[] matA = new char[20];
        char[] alphaChar = new char[26];
        int[] nums = new int[20];
        char[] enc = new char[256];
        words = allowedChar.toCharArray();
        int wordlength = 256;
        int i=0,j=0,k=0;
        String key,salt,date;
        //Define the date format
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHH");
        LocalDateTime dt = LocalDateTime.now();
        date = String.valueOf(dtf.format(dt));
        int somevalue = Integer.parseInt(date);
        somevalue = (somevalue-(somevalue/2))/((somevalue*somevalue)%26);
        dateChar = date.toCharArray();
        alphaChar = alphabets.toCharArray();
        
        for(i=0;i<10;i++){
            try {
                saltChar[i] = alphaChar[((i * somevalue) % 26)];
            }catch (Exception e){
                saltChar[i] = (alphaChar[((i * somevalue) % 26)*(-1)]);
            }
            keyChar[i] = alphaChar[(i*1037/273)%26];
        }
        String temp = String.valueOf(keyChar)+String.valueOf(saltChar);
        matA = temp.toCharArray();
        

        for(j=0;j<20;j++){
               nums[j] = (matA[j]*matA[j]*Integer.parseInt(String.valueOf(dateChar[j%10])))%26;
            }
            
        for(i=0; i<wordlength;i++){
            enc[i] = words[(nums[i%19]*Integer.parseInt(String.valueOf(dateChar[j%10]))+i)%42];
        }        
        
        
        String finalKey="";
        for(i=0;i<enc.length;i++){
            finalKey+=enc[i];
        }
        return finalKey;
    }
}
