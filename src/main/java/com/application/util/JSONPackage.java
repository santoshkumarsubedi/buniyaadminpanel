/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.util;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author accidental-genius
 */
public class JSONPackage {
        public static String getPostDataString(HashMap<String,String> params)
    {
       
                //object for jwtbuilder
                JwtBuilder jwtBuilder = Jwts.builder();
                
                //JWT algorithm for the encryption
                SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
                
                //Date for the signature
                long nowMillis = System.currentTimeMillis();
                Date now = new Date(nowMillis);
                jwtBuilder.setIssuedAt(now);
                //String firstkey=keyGenerator.generateKey();
                String firstkey=KeyGenerator.generateKey();
                params.remove("key");
                //generate signature key
                //SecretKey key = Keys.secretKeyFor(signatureAlgorithm);
                Key signingKey = new SecretKeySpec(firstkey.getBytes(), signatureAlgorithm.getJcaName());
                
                StringBuilder result = new StringBuilder();
                boolean first=true;
                for(Map.Entry<String,String> entry:params.entrySet()){
                    if(first){
                        first = false;
                    }else{
                        
                    }
                    jwtBuilder.claim(entry.getKey(),entry.getValue());
                    
                }
                jwtBuilder.signWith(signingKey);
                
                return jwtBuilder.compact();
         
    }
}
