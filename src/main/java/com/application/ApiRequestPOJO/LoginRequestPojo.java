/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ApiRequestPOJO;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author accidental-genius
 */
public class LoginRequestPojo implements Serializable{
    @SerializedName("username")
    private String username;
    
    @SerializedName("password")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
