/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.UserDetailDAO;
import com.application.Service.UserDetailsService;
import com.application.entity.UserDetails;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */

public class UserDetailServiceImpl implements UserDetailsService{

    private UserDetailDAO userDetailDAO;
    
    public void setUserDetailDAO(UserDetailDAO userDetailDAO){
        this.userDetailDAO = userDetailDAO;
    }
    
    @Override
    @Transactional
    public void insert(UserDetails userDetails) {
        this.userDetailDAO.insert(userDetails);
    }

    @Override
    @Transactional
    public void update(UserDetails userDetails) {
        this.userDetailDAO.update(userDetails);
    }

    @Override
    @Transactional
    public List<UserDetails> list() {
        return this.userDetailDAO.list();
    }

    @Override
    @Transactional
    public UserDetails getDetailByID(int id) {
    return this.userDetailDAO.getDetailByID(id);
    }

    @Override
    @Transactional
    public void remove(int id) {
     this.remove(id);
    }
    
}
