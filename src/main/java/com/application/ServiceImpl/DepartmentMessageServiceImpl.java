/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.DepartmentMessageDAO;
import com.application.Service.DepartmentMessageService;
import com.application.entity.DepartmentMessages;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */
public class DepartmentMessageServiceImpl implements DepartmentMessageService{

    private DepartmentMessageDAO departmentMessageDAO;
    
    public void setDepartmentMessageDAO(DepartmentMessageDAO departmentMessageDAO){
        this.departmentMessageDAO = departmentMessageDAO;
    }
    
    @Transactional
    @Override
    public void insert(DepartmentMessages message) {
        departmentMessageDAO.insert(message);
    }

    @Transactional
    @Override
    public List<DepartmentMessages> getDepartmentById(int id) {
        return departmentMessageDAO.getDepartmentById(id);
    }
    
}
