/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.FileSharingDAO;
import com.application.Service.SharedFileService;
import com.application.entity.SharedFile;
import com.application.entity.Users;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */
public class FileSharingServiceImpl implements SharedFileService{

    private FileSharingDAO fileShareDAO;
    
    @Autowired
    public void setFileShareDAO(FileSharingDAO fileShareDAO){
        this.fileShareDAO = fileShareDAO;
    }
    
    
    @Transactional
    @Override
    public void insert(SharedFile file) {
        fileShareDAO.insert(file);
    }

    @Transactional
    @Override
    public SharedFile getId(int id) {
    return fileShareDAO.getId(id);
    }

    @Transactional
    @Override
    public List<SharedFile> getByUser(Users users) {
    return fileShareDAO.getByUser(users);
    }
    
}
