/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.UserDAO;
import com.application.Service.UserService;
import com.application.entity.Users;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */
public class UserServiceImpl  implements UserService{

    private UserDAO userDao;
    
    public void setUserDao(UserDAO userDao){
        this.userDao = userDao;
    }
    
    @Override
    @Transactional
    public void addUser(Users user) {
       this.userDao.addUser(user);
    }

    @Override
    @Transactional
    public void updatePerson(Users user) {
        this.userDao.UpdatePerson(user);
    }

    @Override
    @Transactional
    public List<Users> listUsers() {
        return this.userDao.listUsers();
    }

    @Override
    @Transactional
    public Users getUserById(int id) {
        
        return this.userDao.getUserById(id);
         
    }

    @Override
    @Transactional
    public void removePerson(int id) {
         this.userDao.removePerson(id);
    }

    @Override
    @Transactional
    public Users getUserByUsername(String username) {
        
        return this.userDao.getUserByUsername(username);
    }

    @Override
    @Transactional
    public Users getUserByDeviceId(String deviceId) {
       return this.userDao.getUserByDeviceId(deviceId);
    }

    @Override
    @Transactional
    public List<Users> getUsersByDepartment(int id) {
       return this.userDao.getUsersByDepartment(id);
    }
    
}
