/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.CallHistoryDAO;
import com.application.Service.CallHistoryService;
import com.application.entity.CallHistory;
import com.application.entity.Users;
import java.util.List;
import javax.transaction.Transactional;

/**
 *
 * @author accidental-genius
 */
public class CallHistoryServiceImpl implements CallHistoryService{

    private CallHistoryDAO callHistoryDAO;
    
    public void setCallHistoryDAO(CallHistoryDAO callHistoryDAO){
        this.callHistoryDAO = callHistoryDAO;
    }
    
    @Transactional
    @Override
    public void insert(CallHistory callHistory) {
       this.callHistoryDAO.insert(callHistory);
    }
    
    @Transactional
    @Override
    public void update(CallHistory callHistory) {
        this.callHistoryDAO.update(callHistory);
    }

    @Transactional
    @Override
    public List<CallHistory> getHistoryByUser(Users user) {
        return this.callHistoryDAO.findHistoryByUsrt(user);
    }
    
}
