/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.DepartmentDAO;
import com.application.Service.DepartmentService;
import com.application.entity.Department;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */
public class DepartmentServiceImpl implements DepartmentService{

    private DepartmentDAO departmentDAO;
    public void setDepartmentDAO(DepartmentDAO departmentDAO){
        this.departmentDAO = departmentDAO;
    }
    
    @Transactional
    @Override
    public void insert(Department department) {
       departmentDAO.insert(department);
    }
    
    @Transactional
    @Override
    public void update(Department department) {
       departmentDAO.update(department);
    }

    @Transactional
    @Override
    public List<Department> listDepartment() {
    return departmentDAO.listDepartment();
    }

    @Transactional
    @Override
    public Department getDepartmentById(int id) {
    return departmentDAO.getDepartmentById(id);
    }

    @Transactional
    @Override
    public void DeleteById(int id) {
        departmentDAO.DeleteById(id);
    }

    @Transactional
    @Override
    public Department getDepartmentByName(String departmentName) {
        return departmentDAO.getDepartmentByName(departmentName);
    }
    
}
