/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.BugReportDAO;
import com.application.Service.BugReportService;
import com.application.entity.BugReport;
import com.application.entity.CallHistory;
import com.application.entity.Users;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */
public class BugReportServiceImpl implements BugReportService{
    
    private BugReportDAO bugReportDAO;
    
    public void setBugReportDAO(BugReportDAO bugReportDAO){
        this.bugReportDAO = bugReportDAO;
    }

    @Transactional
    @Override
    public void insert(BugReport bugReport) {
        bugReportDAO.insert(bugReport);
    }

    @Transactional
    @Override
    public void update(BugReport bugReport) {
        bugReportDAO.update(bugReport);
    }

    @Transactional
    @Override
    public List<BugReport> getReports() {
        return bugReportDAO.getReports();
    }

    
}
