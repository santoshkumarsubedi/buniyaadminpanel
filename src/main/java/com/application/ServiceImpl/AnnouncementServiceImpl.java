/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.AnnouncementDAO;
import com.application.Service.AnnouncementService;
import com.application.entity.Announcement;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */
public class AnnouncementServiceImpl implements AnnouncementService{

    private AnnouncementDAO announcementDAO;
    
    public void setAnnouncementDAO(AnnouncementDAO announcementDAO){
        this.announcementDAO = announcementDAO;
    }
    
    @Transactional
    @Override
    public void insert(Announcement announcement) {
       announcementDAO.insert(announcement);
    }

    @Transactional
    @Override
    public void update(Announcement announcement) {
        announcementDAO.update(announcement);
    }

    @Transactional
    @Override
    public List<Announcement> getAllAnnouncement() {
        return announcementDAO.getAllAnnouncement();
    }

    @Transactional
    @Override
    public Announcement getAnnouncementById(int id) {
        return announcementDAO.getAnnouncementById(id);
    }
    
}
