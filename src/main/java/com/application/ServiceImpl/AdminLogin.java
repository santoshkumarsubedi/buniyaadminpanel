/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.AdminDao;
import com.application.entity.Admin;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */
public class AdminLogin implements UserDetailsService{
    private AdminDao adminDao;
    
    @Autowired
    public void setAdminDao(AdminDao adminDao){
        this.adminDao=adminDao;
    }
    
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
        Admin admin = adminDao.findByUserName(string);
        List<GrantedAuthority> authorities = buildUserAuthority(admin.getRole());
        
        
        return buildUserForAuthentication(admin, authorities);
    }
    
    private User buildUserForAuthentication(Admin admin,List<GrantedAuthority> authorities){
        return new User(admin.getUsername(),admin.getPassword(),
                        true,true,true,true,authorities);
                }
    
    private List<GrantedAuthority> buildUserAuthority(String userRoles){
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        setAuths.add(new SimpleGrantedAuthority(userRoles));
        
		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(setAuths);
                return result;
    }
    
}
