/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.ServiceImpl;

import com.application.DAO.PersonalMessageDao;
import com.application.Service.PersonalMessageService;
import com.application.entity.PersonalMessage;
import com.application.entity.Users;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author accidental-genius
 */
public class PersonalMessageServiceImpl implements PersonalMessageService{

    private PersonalMessageDao personalMessageDao;
    
    public void setPersonalMessageDao(PersonalMessageDao personalMessageDao){
        this.personalMessageDao = personalMessageDao;
    }
    
    @Transactional
    @Override
    public void addMessage(PersonalMessage message) {
        personalMessageDao.addMessage(message);
    }

    @Transactional
    @Override
    public void UpdateMessage(PersonalMessage message) {
       personalMessageDao.UpdateMessage(message);
       
    }

    @Transactional
    @Override
    public List<PersonalMessage> getMessagesByUser(Users user) {
        return personalMessageDao.getMessagesByUser(user);
    }
    
}
