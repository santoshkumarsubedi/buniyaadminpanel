/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var users=[];
var liveCall=[];
$(document).ready(function(){
const socketUrl = 'http://localhost:3000';
  socket = io(socketUrl);

socket.on('management',function(msg){
    var data=JSON.parse(msg);   
    if(data[0].Command==='Login'){
        if(data[0].Status===true){
           $("#Signaling_Server").text('ON'); 
        }else{
            $("#Signaling_Server").text('OFF'); 
        }
    }else if(data[0].Command==='ServerStatus'){
        if(data[0].ServerName==="CommunicationServer"){
             if(data[0].ServerStatus===true){
           $("#Connection_Server").text('ON'); 
        }else{
            $("#Connection_Server").text('OFF'); 
        }
        }
    }else if(data[0].Command==="UserConnection"){
        if(data[0].Status==="Connected"){
           addUser(data);   
        }else if(data[0].Status==="Disconnected"){
                removeUser(data[0]);       
        }
    }else if(data[0].Command==="LiveCall"){
        if(data[0].Status==="Connected"){
            addCall(data);
        }else if(data[0].Status==="Disconnected"){
            removeCall(data);
        }
    }
    
});


socket.on('disconnect',function(msg){
    
    $("#Signaling_Server").text('OFF'); 
});

socket.on('connect',function(msg){
    $("#online-box").html();
    socket.emit('join_management',"{\"username\":\"AdminPanel\", \"password\":\"pass123\"}");
    socket.emit('management',"{\"status\":\"command\", \"server_status\":\"CommunicationServer\",\"username\":\"AdminPanel\"}");
    socket.emit('management',"{\"status\":\"users\"}");
});     
});
        
function addUser(datas){
    
    for(j=0;j<datas.length;j++){
        var contains=false;
        var data=datas[j];
        //console.log(j);
    for(i=0;i<users.length;i++){
          var user=users[i];
          if(data.UserId==user.UserId){
              contains=true;
          }
    }
    if(!contains){
        users.push({"Username":data.Username,"UserId":data.UserId,"Fullname":data.Fullname});
        refreshOnlineList();
    }
}
}

function addCall(datas){
    var contains = false;
    for(j=0;j<datas.length;j++){
        var data = datas[j];
        for(i=0;i<liveCall.length;i++){
            var call = liveCall[i];
            if(call.SessionId===data.SessionId){
                contains = true;
            }
        }
        
        if(!contains){
            liveCall.push({"SessionId":data.SessionId,"Receiver":data.Receiver,"Caller":data.Caller});
            refreshLiveCall();
        }
    }
}

function removeUser(data){
       var contains=false;
       var index;
    for(i=0;i<users.length;i++){
          var user=users[i];
          if(data.UserId==user.UserId){
              contains=true;
          }
    }
    
    if(contains){
        users.splice(index,1);
        refreshOnlineList();
    }
}

function refreshOnlineList(){
    $("#online-box").html("");
    $("#online-count").html(users.length);
        for(i=0;i<users.length;i++){
          var data=users[i];
           $("#online-box").append("    <div class=\"row\" id=\""+data.UserId+"\">"+
                          "<div class=\"col-xl-5 col-lg-6\">"+data.Fullname+"</div>"+
                          "<div class=\"col-xl-2 col-lg-2\"><div class=\"circle-red\"></div></div>"+
                          "<div class=\"col-xl-3 col-lg-3\">1 min</div>"+
                      "</div>");
    }
}

function refreshLiveCall(){
    $("#online-call").html("");
    //$("#online-call").html(liveCall.len)
    for(i=0;i<liveCall.length;i++){
        var data = liveCall[i];
        $("#online-call").append("<div class=\"row\" id=\""+data.SessionId+"\">"+
                          "<div class=\"col-xl-5 col-lg-6\">"+data.SessionId+"</div>"+
                          "<div class=\"col-xl-3 col-lg-3\">"+data.Caller+"</div>"+
                          "<div class=\"col-xl-3 col-lg-3\">"+data.Receiver+"</div>"+
                          "</div>");
    }
    
    function removeCall(data){
        
    }
}