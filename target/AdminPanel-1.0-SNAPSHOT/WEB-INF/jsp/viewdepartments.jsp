<%-- 
    Document   : viewdepartments
    Created on : Nov 3, 2019, 12:17:30 PM
    Author     : accidental-genius
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<t:wrapper>
    <div class="card-body">
        <a href="/AdminPanel/department/add">Add Department</a>
    </div>
 <div class="card-body">
              <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            </thead>
               <tfoot>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            </tfoot>
            <tbody>
            <c:forEach items="${displayDepartments}" var="p">
                <tr>
                    <td><c:out value="${p.id}"/></td>
                    <td><c:out value="${p.name}"/></td>
                    <td><a href="/AdminPanel/department/edit?id=${p.id}">Edit</a></td>
                    <td><a href="/AdminPanel/department/delete?id=${p.id}">Delete</a></td>
                </tr>
            </c:forEach>
                </tbody>
        </table>
              </div>
 </div>
                </t:wrapper>