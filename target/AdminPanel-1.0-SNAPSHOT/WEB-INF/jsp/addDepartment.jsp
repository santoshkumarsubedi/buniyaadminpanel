<%-- 
    Document   : addDepartment
    Created on : Nov 2, 2019, 12:35:19 PM
    Author     : accidental-genius
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<t:wrapper>
    <h1>Add Department</h1>
    <form:form action="addDep" method="POST" modelAttribute="department">
            <form:input path="id" readonly="true" disabled="true" hidden="true"/>
            <label>Name</label>
            <form:input path="name"/><br>
            
            <input type="submit" value="Submit"/>
        </form:form>
</t:wrapper>