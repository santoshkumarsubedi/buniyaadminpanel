<%-- 
    Document   : addAnnouncement
    Created on : Feb 23, 2020, 10:54:43 PM
    Author     : accidental-genius
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
    <body>
        <h1>Add Announcement</h1>
        <form:form action="addAnnouncement" method="POST" modelAttribute="announcement" name="addAnnouncementForm">
            <form:input path="id" readonly="true" disabled="true" hidden="true"/>
            <form:label path="subject">Subject</form:label><br>
            <form:input path="subject"/><br>
            <form:label path="notice">Announcement</form:label><br>
            <form:input path="notice"/><br>
            <input type="submit" value="Submit" id="addAnnouncementFormSubmit"/>
        </form:form>
            <script src="resources/js/ajax.js"></script>
    </body>
