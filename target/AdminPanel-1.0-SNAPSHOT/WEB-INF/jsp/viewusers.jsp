<%-- 
    Document   : viewusers
    Created on : Nov 1, 2019, 8:41:55 AM
    Author     : accidental-genius
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
 <div class="card-body">
              <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <td>User Id</td>
                <td>Username</td>
                <td>Department</td>
                <td>Activation Status</td>
                <td>Password Status</td>
                <td>Mac Address</td>
                <td>Reset Account</td>
                <td> Edit</td>
            </tr>
            </thead>
               <tfoot>
            <tr>
                <td>User Id</td>
                <td>Username</td>
                <td>Department</td>
                <td>Activation Status</td>
                <td>Password Status</td>
                <td>Mac Address</td>
                <td>Reset Account</td>
                <td> Edit</td>
            </tr>
            </tfoot>
            <tbody>
            <c:forEach items="${displayUsers}" var="p">
                <tr>
                    <td><c:out value="${p.id}"/></td>
                    <td><c:out value="${p.username}"/></td>
                    <td><c:out value="${p.department.name}"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${p.activated==true}">
                                Activated
                            </c:when>
                            <c:otherwise>
                                Not Activated
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                      <c:choose>
                            <c:when test="${p.passwordChanged==true}">
                                Changed
                            </c:when>
                            <c:otherwise>
                                Not Changed
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${p.macAddress==null}">
                                Account Not Loged in 
                            </c:when>
                            <c:otherwise>
                                <c:out value="${p.macAddress}"></c:out>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td><a href="/AdminPanel/user/reset?id=${p.id}">Reset</a></td>
                    <td><a href="/AdminPanel/user/edit?id=${p.id}">Edit</a></td>
                </tr>
            </c:forEach>
                </tbody>
        </table>
              </div>
 </div>

<script>
    $(document).ready( function () {
    $('#dataTable').DataTable();
} );
    
</script>