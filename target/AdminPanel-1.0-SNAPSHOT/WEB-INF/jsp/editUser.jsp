<%-- 
    Document   : addUser
    Created on : Oct 31, 2019, 11:16:51 AM
    Author     : accidental-genius
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<t:wrapper>
<!DOCTYPE html>
    <body>
        <h1>Add User</h1>
        <form:form action="/AdminPanel/user/editUser" method="POST" modelAttribute="user" name="addUserForm">
            <form:input path="id" readonly="true" hidden="true"/>
            <form:label path="username">Username</form:label>
            <form:input path="username"/><br>
            <form:label path="department_id">Department</form:label>
            <form:select path="department_id">
                <c:forEach items="${departments}" var="department">
                    <form:option value="${department.id}">${department.name}</form:option>
                </c:forEach>
            </form:select><br>
            <form:label path="firstName">First Name</form:label>
            <form:input path="firstName"/><br>
            <form:label path="middleName">Middle Name</form:label>
            <form:input path="middleName"/><br>
            <form:label path="lastName">Last Name</form:label>
            <form:input path="lastName"/><br>
            <form:label path="email">Email</form:label>
            <form:input path="email"/><br>
            <form:label path="contactNumber">Contact</form:label>
            <form:input path="contactNumber"/><br>
            <form:label path="address">Address</form:label>
            <form:input path="address"/><br>
            <input type="submit" value="Submit" id="addUserFormSubmit"/>
        </form:form>     
            
            <script src="resources/js/ajax.js"></script>
    </body>
    </t:wrapper>